package xyz.rex9k.chattoggle;

import net.minecraft.client.settings.GameSettings;
import org.lwjgl.input.Mouse;
import org.lwjgl.input.Keyboard;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.opengl.GL11;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraft.client.settings.KeyBinding;
import java.util.TimerTask;
import java.util.Timer;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "chattoggle", version = "1.0")
public class ChatToggle
{
    private boolean chatEnabled;
    private boolean animationFinished;
    private float offset;
    private Timer timer;
    private TimerTask activeTask;
    private KeyBinding key;
    private long nextToggle;
    
    public ChatToggle() {
        this.chatEnabled = true;
        this.timer = new Timer();
        this.key = new KeyBinding("Toggle Chat", 38, "Toggle Chat Mod");
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent e) {
        MinecraftForge.EVENT_BUS.register((Object)this);
        FMLCommonHandler.instance().bus().register((Object)this);
        ClientRegistry.registerKeyBinding(this.key);
    }
    
    @SubscribeEvent
    public void onRenderGui(final RenderGameOverlayEvent.Pre e) {
        if (e.getType() == RenderGameOverlayEvent.ElementType.CHAT) {
            if (this.animationFinished) {
                e.setCanceled(!this.chatEnabled);
            }
            else {
                GL11.glPushMatrix();
                GL11.glTranslatef(this.offset, 0.0f, 0.0f);
            }
        }
    }
    
    @SubscribeEvent
    public void onRenderGui(final RenderGameOverlayEvent.Post e) {
        if (e.getType() == RenderGameOverlayEvent.ElementType.CHAT && !this.animationFinished) {
            GL11.glPopMatrix();
        }
    }
    
    @SubscribeEvent
    public void onKeyPress(final InputEvent.KeyInputEvent e) {
        if ((Minecraft.getMinecraft().gameSettings.keyBindChat.isKeyDown() && !this.chatEnabled) || (Keyboard.getEventKey() == this.key.getKeyCode() && Keyboard.getEventKeyState() && System.currentTimeMillis() > this.nextToggle)) {
            this.nextToggle = System.currentTimeMillis();
            this.toggleChat();
        }
    }
    
    private void toggleChat() {
        this.animationFinished = false;
        if (this.activeTask != null) {
            this.activeTask.cancel();
        }
        this.chatEnabled = !this.chatEnabled;
        if (this.chatEnabled) {
            this.timer.scheduleAtFixedRate(this.activeTask = new TimerTask() {
                @Override
                public void run() {
                    final float finalOffset = 0.0f;
                    final float delta = 40.0f;
                    if (ChatToggle.this.offset + 40.0f > 0.0f) {
                        ChatToggle.this.offset = 0.0f;
                        this.cancel();
                        ChatToggle.this.activeTask = null;
                        ChatToggle.this.animationFinished = true;
                    }
                    else {
                        ChatToggle.this.offset += 40.0f;
                    }
                }
            }, 0L, 16L);
        }
        else {
            this.timer.scheduleAtFixedRate(this.activeTask = new TimerTask() {
                @Override
                public void run() {
                    final GameSettings settings = Minecraft.getMinecraft().gameSettings;
                    final float finalOffset = 320.0f * -settings.chatWidth * settings.chatScale;
                    final float delta = 40.0f;
                    if (ChatToggle.this.offset - 40.0f < finalOffset) {
                        ChatToggle.this.offset = finalOffset;
                        this.cancel();
                        ChatToggle.this.activeTask = null;
                        ChatToggle.this.animationFinished = true;
                    }
                    else {
                        ChatToggle.this.offset -= 40.0f;
                    }
                }
            }, 0L, 16L);
        }
    }
}
